tilp2 (1.18-4) UNRELEASED; urgency=medium

  [ John Scott ]
  * Update debian/copyright because AppStream data is CC0 (thanks Lintian)
  * Move debian/watch to GitHub repository

 -- Andreas B. Mundt <andi@debian.org>  Sat, 04 Jan 2020 20:41:55 -0500

tilp2 (1.18-3) unstable; urgency=medium

  * Update german l10n.  (Closes: #588411).

 -- Andreas B. Mundt <andi@debian.org>  Sun, 25 Nov 2018 21:57:23 +0300

tilp2 (1.18-2) unstable; urgency=medium

  * Add sensible-utils as dependency.
  * Bump Standards-Version to 4.2.1 (no changes needed).

 -- Andreas B. Mundt <andi@debian.org>  Wed, 03 Oct 2018 08:39:06 +0300

tilp2 (1.18-1) unstable; urgency=medium

  * Bump Standards-Version to 4.0.1 (no changes needed).
  * New upstream version 1.18
  * Update Vcs-* links to 'salsa.debian.org'.
  * Bump Standards-Version to 4.1.4 (no changes needed).
  * Bump compatibility level to 11.
  * Update debian/rules and dependencies for new upstream release.
  * Fix outdated KDE configure stuff.
  * DEP-5 copyright and update 'debian/watch'.
  * Fix appstream and desktop metadata.

 -- Andreas B. Mundt <andi@debian.org>  Sat, 19 May 2018 10:04:24 +0300

tilp2 (1.17-3) unstable; urgency=medium

  * Fix debian/watch to find correct upstream version.
  * Use secure URL for Vcs-Git field.
  * Bump Standards-Version to 4.0.0 (no changes needed).

 -- Andreas B. Mundt <andi@debian.org>  Tue, 15 Aug 2017 21:12:05 +0200

tilp2 (1.17-2) unstable; urgency=medium

  * Switch to secure URI for vcs-field.
  * Remove deprecated menu file.
  * Bump Standards-Version to 3.9.8 (no changes needed).

 -- Andreas B. Mundt <andi@debian.org>  Sat, 03 Dec 2016 20:16:26 +0300

tilp2 (1.17-1) unstable; urgency=low

  * Team maintained in Debian-Science now (closes: #678059).
    Modifications in cooperation with Albert Huang
    <alberth.debian@gmail.com>, thanks!
  * New upstream release.
  * Bumped versions of libti* dependencies.
  * Add debian/watch to enable upstream release tracking.
  * Switch to packaging format 3.0 (quilt).
  * Bump 'debian/compat' to 9 and Standards-Version to 3.9.4.

 -- Andreas B. Mundt <andi@debian.org>  Sat, 10 Aug 2013 19:57:42 +0200

tilp2 (1.12-1) unstable; urgency=low

  * Initial release. (Closes: #503099)

 -- Krzysztof Burghardt <krzysztof@burghardt.pl>  Tue, 09 Jun 2009 20:03:57 +0200
